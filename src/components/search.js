import React, { useState } from 'react';
import { connect } from "react-redux";
import { getMovies } from '../redux/movies/actions'

const Search = (props) => {
    const { getMovies } = props;
    const [keyword, setKeyword] = useState();

    const onSubmitSearch = () => {
        getMovies(keyword)
    };

    return (
        <div className="d-flex header-searchbar" >
            <input
                className="form-control mr-2 searchbar-input"
                onKeyPress={e => { if (e.key === 'Enter') { onSubmitSearch() } }}
                onChange={e => { setKeyword(e.target.value) }}
                placeholder="Search your favorite movies"
                aria-label="Search your favorite movies" />
            <button className="btn searchbar-button" onClick={() => onSubmitSearch()}>Search</button>
        </div >
    );
};

const mapStateToProps = (state) => {
    return ({
    });
};

const mapDispatchToProps = (dispatch) => ({
    getMovies: (keyword) => dispatch(getMovies(keyword)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);