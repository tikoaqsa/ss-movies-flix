import React from 'react';

const renderLoading = () => (
    <div className="vw-100 vh-100 d-flex justify-content-center">
        <div className="spinner-grow text-white" role="status">
        </div>
        <span style={{ color: '#ced4da', margin: '5px 0 0 10px' }}>Loading...</span>
    </div>
)

export default renderLoading;