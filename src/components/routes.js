import Movies from './movies'

const routes = [
    { name: "Movies", component: Movies },
];

export default routes;