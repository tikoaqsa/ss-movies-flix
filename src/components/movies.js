import React, { useEffect } from 'react';
import { connect } from "react-redux";
import { getMovies } from '../redux/movies/actions'
import renderLoading from './loading'

const Movies = (props) => {
    const { getMovies, listMovie } = props;

    useEffect(() => {
        setTimeout(function () {
            getMovies('man')
        }, 4000);
    }, []) //eslint-disable-line

    return (
        <div className='movies'>
            {listMovie && listMovie.length > 0 ? listMovie.map((data, index) => (
                <div className="movies-card card" key={index}>
                    <img className="card-img-top movies-cover" src={data.Poster} alt={`poster - ${data.Title}`} />
                    <div className="card-body">
                        <p className="movies-title">{data.Title}</p>
                    </div>
                </div>
            )) : listMovie && listMovie.status === 'error' ? <div className='text-white'>{listMovie.message}</div> : renderLoading()}
        </div>
    )
}

const mapStateToProps = (state) => {
    return ({
        listMovie: state.movies.listMovie,
    })
}

const mapDispatchToProps = (dispatch) => ({
    getMovies: (keyword) => dispatch(getMovies(keyword)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Movies);