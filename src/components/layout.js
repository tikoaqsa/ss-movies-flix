import React, { Suspense } from 'react';
import Header from './header';
import Search from './search';
import routes from './routes';
import renderLoading from './loading';

const Layout = () => {

    return (
        <div className="app">
            <Suspense fallback={renderLoading()}>
                <header>
                    <Header />
                </header>
                <main className='main'>
                    {routes.map((routes, index) => (
                        <React.Fragment key={index}>
                            <div id={routes.key} className='container'>
                                <Search />
                                <routes.component />
                            </div>
                        </React.Fragment>
                    ))}
                </main>
            </Suspense>
        </div>
    )
}

export default Layout;
