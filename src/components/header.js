import React from 'react';

const Header = () => {
    return (
        <nav className="navbar header">
            <div className="container-fluid justify-content-center">
                <div className="header-brand">Movies-flix</div>
            </div>
        </nav>
    )
}

export default Header;