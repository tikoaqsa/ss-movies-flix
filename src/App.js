import React from 'react';
import './App.scss';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Layout from './components/layout';

const App = () => {
  return (
    <BrowserRouter>
      <React.Suspense>
        <Switch>
          <Route path="*" name="Welcome!" render={(props) => <Layout {...props} />} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  )
}

export default App;
