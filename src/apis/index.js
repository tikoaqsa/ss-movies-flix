import axios from 'axios';

const apiUrl = 'http://www.omdbapi.com';
const apiKey = '35d9daca';

export const getListMovie = (keyword) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'GET',
            url: `${apiUrl}/?apikey=${apiKey}&s=${keyword}`,
            timeout: 1000 * 10, // Wait for 10 seconds
        })
            .then(function (response) {
                if (response.status === 200 && response.data.Response === 'True') {
                    resolve(response.data.Search)
                } else {
                    resolve({ status: 'error', message: response.data.Error })
                }
            })
            .catch(function (error) {
                console.log(error);
                reject(error);
            });
    });
};

export const getMovieDetail = (imdbCode, type) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'GET',
            url: `${apiUrl}/?apikey=${apiKey}&i=${imdbCode}&type=${type}`,
            timeout: 1000 * 10, // Wait for 10 seconds
        })
            .then(function (response) {
                if (response.data.success) {
                    resolve(response.data)
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });
};