import {
    MOVIE,
} from './types';

const initialState = {
    listMovie: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case MOVIE: {
            return {
                ...state,
                listMovie: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}

export default reducer;
