import { MOVIE } from './types';
import { getListMovie } from '../../apis/index';

export const listMovie = (data) => (
    {
        type: MOVIE,
        payload: data,
    }
);

export const getMovies = (keyword) => async (dispatch) => {
    try {
        await getListMovie(keyword)
            .then((response) => {
                dispatch(listMovie(response))
            })
            .catch((error) => {
                console.log(error.message)
                dispatch(listMovie(''))
            });
    } catch (error) {
        console.log(error.message)
        dispatch(listMovie([]));
    }
};
